# Varnish Cache Manager

    HOST=host1.com, host2.com npm start

# Docker 
	docker build -t host.com/cache-manager .
	docker push host.com/cache-manager

# Kubernetes 
	kubectl create ns test

	kubectl apply -f k8s/web-service.yml
