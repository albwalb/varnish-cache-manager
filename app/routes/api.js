const express = require('express');
const router = express.Router();
const request = require('request');

const logs = [];
var errori = null;

const password = 'Basic ' + Buffer.from(process.env.USERNAME + ':' + process.env.PASSWORD).toString('base64');
router.get('/', function(req, res, next) {
    res.render('logs', { title: 'API Entry Point', message: "Routes avilable: /api/purge" });
});



router.post('/purge', function(req, res, next) {
    var errore = null;
    const host = process.env.HOST.split(',');
    const domain = req.body.domain;
    console.log(req.body);
    if(!req.body.host && !req.body.domain){
        res.status(500).json({'error': 'You need to specify host and domain to clear the cache.'});
    } else {
        if(!req.body.data){
        host.forEach(function(singleHost){
            var hst = singleHost;
            request.post({
                headers: {'content-type' : 'application/x-www-form-urlencoded', "Authorization":password},
                url:     `https://${singleHost}/ban`,
                body:    `req.http.host == ${domain}`
              }, function(error, response, body){
                if(response){
                    console.log(`${hst} requests for: ${domain} status: ${response.statusCode}`);
                    logs.push(response.statusCode);
                }
                if(response.statusCode!=200){
                    errori =1;
                }
            });
        })
        
        } else {
            host.forEach(function(singleHost){
            var hst = singleHost;
             request.post({
                headers: {'content-type' : 'application/x-www-form-urlencoded', "Authorization":password},
                url:     `https://${singleHost}/ban`,
                body:    `req.http.host == ${domain} && req.url ~ ${req.body.data}`
              }, function(error, response, body){
                if(response){
                    console.log(`${hst}  requests for: ${domain}, path: ${req.body.data}, status: ${response.statusCode}`);
                    logs.push(response.statusCode);
                    if(response.statusCode!=200){
                        errori = 1;
                    }
                } 
              });
            });
        }
        if(errori){
            res.status(500).json({status: "Errors on cache hosts"});
        } else {
            res.status(200).json({status: "Deleted cache on all hosts"});
        }
        //errore ? res.status(500).json({"error":"There has been an error in posting the data to the cache server"}) : res.render('logs', { title: 'Success', message: `Deleted cache on all hosts` });
    }
    

});
  
module.exports = router;