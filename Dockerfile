FROM node:8.0

MAINTAINER Alberto Fulcini <a.fulcini@criticalcase.com>

# If you want SIGTERM to work properly in Docker with npm start
RUN wget https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64.deb
RUN dpkg -i dumb-init_*.deb

# Create app directory
RUN mkdir -p /app
WORKDIR /app

# Install app dependencies
COPY app /app
RUN npm install

EXPOSE 3000

CMD ["dumb-init", "npm", "start"]
